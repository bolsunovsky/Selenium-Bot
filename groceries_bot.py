
import sys
import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

#login credentials
username = 'yourloginhere'
password = 'password'

options = Options()

options.add_experimental_option("excludeSwitches", ['enable-automation'])

options.add_experimental_option("prefs", {
     
    "profile.default_content_setting_values.notifications" : 1 ,                                       
    "profile.password_manager_enabled" : False, 
    "credentials_enable_service": False
    })

driver = webdriver.Chrome(options=options)
driver.get('https://www.tesco.com/groceries/en-GB/')

driver.set_window_position(0, 0)


login_button = driver.find_elements_by_class_name('styled__Anchor-sc-1xizymv-0')
login_button[1].click()



username_field = driver.find_element_by_id('username')
password_field = driver.find_element_by_id('password')

username_field.click()
username_field.send_keys(username)

password_field.click()
password_field.send_keys(password)


# click the sign in button
sign_in_button = driver.find_elements_by_class_name('ui-component__button')
sign_in_button[0].click()

time.sleep(3)


# delete all items in the basket
delete_items = driver.find_elements_by_class_name('delete-single-item') 
[item.click() for item in delete_items]

time.sleep(1)



# search for milk explicitly
search_field = driver.find_elements_by_id('search-input')[0]
search_field.click()
search_field.send_keys('milk')

time.sleep(3)

driver.find_element_by_xpath('//button[contains(@class, "search-bar__submit") and contains(@class, "icon-search-white")]').click()


time.sleep(3)


driver.find_elements_by_xpath('//button[contains(@class, "button") and contains(@class, "add-control")]')[0].click()
time.sleep(1)
driver.find_elements_by_xpath('//button[contains(@class, "button") and contains(@class, "add-control")]')[1].click()
time.sleep(1)
driver.find_elements_by_xpath('//button[contains(@class, "button") and contains(@class, "add-control")]')[2].click()
time.sleep(5)


driver.find_element_by_xpath('//a[@href="/groceries/en-GB/slots"]').click()

time.sleep(5)



# book home delivery slot
driver.find_element_by_xpath("//a[@href='/groceries/en-GB/slots/delivery']").click()

time.sleep(10)

# check delivery date blocks for availability and print out the times
print('Dates and times available for delivery:')
for element in driver.find_elements_by_xpath('//li[contains(@class, "tabheader") and contains(@class, "slot-selector--week-tabheader") and contains(@class, "slot-selector--3-week-tab-space")]'):
    element.click()
    time.sleep(5)
    
    for e in driver.find_elements_by_xpath('//button[contains(@class, "available-slot--button") and contains(@class, "available-slot--button-oop808")]'):
        t = e.text
        print(t[0:t.find('.')])





